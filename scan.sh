#!/bin/bash

# http://blog.mro.name/2009/07/sane-batch-scan-workaroun/
echo -e "Connect the scanner and power it on!\nPress <RETURN> to continue and <l> + <RETURN> to list connected devices!"
read key_list
  if [[ $key_list == "l" || $key_list == "L" ]] ; then
    echo -e "\nThe first device will be used automatically by scanimage:"
    scanimage -L
    echo ""
  fi
echo ""
count=`ls -1 *.jpg 2>/dev/null | wc -l`
if [ $count != 0 ]; then 
  rm *.jpg
fi

for ((i=0;i < 99999;i++))
# Causes problems when a user wants to scan more than 99999 documents in one sesssion.
do
    file_name=scan-`date "+%Y-%m-%dT%H:%M:%S"`
    # Causes problems if the user scans 2 documents in the same minute. This is realistic, so i go with seconds.
    for ((i=0;i < 99999;i++))
    # Causes problems when a user wants to scan more than 99999 sites for one document.
    do
      echo "Place document no. $i on the scanners feeder."
      echo "Press <RETURN> to continue, <q> + <RETURN> to quit and continue with the next document."
      read key
      if [[ $key == "q" || $key == "Q" ]] ; then
          break
      fi
      destination=$file_name-page$(printf %03d $i)
      scanimage "--device-name=brother5:bus6;dev3" --resolution=200 -l 0 -t 0 --format=tiff --progress | convert tiff:- -trim +repage $destination.jpg <- fix unpaper which uses tiffs
      #PAGE_OFFSET=$(convert $destination.jpg -morphology Dilate:3 Diamond:3,5 -fuzz 10% -trim -format '%xx%h%O' info:-)
      #convert $destination.jpg -trim +repage $destination-trimmed.jpg
    done
    convert *.jpg -despeckle -compress JPEG $file_name.pdf
    echo "File written to $file_name.pdf"
    # TODO?
    # Compress the pdf
    # gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=compressed_PDF_file.pdf input_PDF_file.pdf
    count=`ls -1 *.jpg 2>/dev/null | wc -l`
    if [ $count != 0 ]; then 
     rm *.jpg
    fi 
    echo "Document finished. Press <RETURN> to start a new document, press <a> + <RETURN> to abort the whole document scanning process!"
    read key    
    if [[ -n $(echo *.jpg) ]] ; then
        break
    fi
done

#unpaper
# after testing unpaper i am not satisfied at all. I will add a screenshot to show what it "tried to improve"
